import * as actionTypes from '../Redux/Types';

const initialState = {
    todos: [],
    isLoading: false,
    error: null,
};


export const todosReducers = (state = initialState, action) => {

    switch (action.type) {
        case actionTypes.ADDNEW_TODO:
            // return [
            //     ...state,
            //     action.payload
            // ]
            return {
                ...state,
                isLoading: false,
                todos: [
                    ...state.todos,
                    action.payload
                ],
            };

        case actionTypes.GETALL_TODO:
            // console.log(action.payload)
            // return action.payload
            return {
                ...state,
                isLoading: false,
                todos: action.payload,
            };


        case actionTypes.UPDATE_TODO:
            // console.log(action.payload)
            // return state.map((ele) => (
            //     ele.id === action.payload.id ? action.payload : ele
            // ));
            const updatedTodos = state.todos.map((ele) => (
                ele.id === action.payload.id ? action.payload : ele
            ));
            return {
                ...state,
                isLoading: false,
                todos: updatedTodos,
            };

        case actionTypes.DELETE_TODO:
            console.log(action.payload)
            // return state.filter(todo => todo.id !== action.payload.id);

            const filteredTodos = state.todos.filter(todo => todo.id !== action.payload.id);
            return {    
                ...state,
                isLoading: false,
                todos: filteredTodos,
            };

        case actionTypes.LOADING_TODO:
            return {
                ...state,
                isLoading: true,
                error: null,
            };

        case actionTypes.ERROR_TODO:
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            };
        default:
            return state;
    }
}