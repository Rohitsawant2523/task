import axios from "axios";
import { ADDNEW_TODO, GETALL_TODO, UPDATE_TODO, DELETE_TODO, LOADING_TODO } from "./Types";



export const addNewTodo = (data) => async (dispatch) => {
    const response = await fetch("https://64e34f09bac46e480e789213.mockapi.io/user", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
    }
    );

    try {
        const result = await response.json();
        console.log(result)
        dispatch({ type: ADDNEW_TODO, payload: result });
    } catch (error) {
        return rejectWithValue(error);
    }
}

export const getAllTodos = () => async (dispatch) => {
    try {
        dispatch({ type: LOADING_TODO })
        const response = await axios.get('https://64e34f09bac46e480e789213.mockapi.io/user');
        console.log(response.data)
        dispatch({ type: GETALL_TODO, payload: response.data });
    } catch (error) {
        console.log('Error while calling getAllTodos API', error.message);
    }
}

export const updateTodo = (data, ids) => async (dispatch) => {
    console.log(data)
    const response = await fetch(
        `https://64e34f09bac46e480e789213.mockapi.io/user/${ids}`,
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        }
    )
    try {
        const res = await response.json();
        console.log(res)
        dispatch({ type: UPDATE_TODO, payload: res });

    } catch (error) {
        console.log(error)
    }
}

export const deleteTodo = (id) => async (dispatch) => {
    console.log(id)
    try {
        const response = await axios.delete(`https://64e34f09bac46e480e789213.mockapi.io/user/${id}`,);
        console.log(response)
        console.log('User deleted successfully:', response.data);
        dispatch({ type: DELETE_TODO, payload: response.data });

    } catch (error) {
        console.error('Error deleting user:', error);
    }
}
