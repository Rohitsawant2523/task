import { configureStore, combineReducers } from "@reduxjs/toolkit";
import logger from "redux-logger";
import thunk from "redux-thunk";

import { todosReducers } from './Reducer';

const reducer = combineReducers({
    todos: todosReducers,
})
const store = configureStore({ reducer: reducer, middleware: [thunk, logger] })


export default store;