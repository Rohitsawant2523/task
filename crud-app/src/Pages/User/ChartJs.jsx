import React, { useState } from 'react';
import Chart from "chart.js/auto";

import { CategoryScale } from "chart.js";

Chart.register(CategoryScale);

const data = {
    labels: ['Red', 'Orange', 'Blue'],
    datasets: [
        {
            label: 'Popularity of colours',
            data: [55, 23, 96],
            backgroundColor: [
                'rgba(255, 255, 255, 0.6)',
                'rgba(255, 255, 255, 0.6)',
                'rgba(255, 255, 255, 0.6)'
            ],
            borderWidth: 1,
        }
    ]
}


const ChartJs = () => {
    const [chartData, setChartData] = useState({
        labels: data.map((data) => data.year),
        datasets: [
            {
                label: "Users Gained ",
                data: Data.map((data) => data.userGain),
                //     backgroundColor: [
                //         "rgba(75,192,192,1)",
                //   & quot; #ecf0f1",
                //   "#50AF95",
                //     "#f3ba2f",
                //     "#2a71d0"
                // ],
                borderColor: "black",
                borderWidth: 2
            }
        ]
    });

    return (
        <>
            <div className="App">
                <p>Using Chart.js in React</p>
            </div>
        </>
    )
}

export default ChartJs