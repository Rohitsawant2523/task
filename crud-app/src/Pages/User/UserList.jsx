import React, { useEffect, useState } from 'react'
import axios from "axios";
import { Link } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import AddUpdateModal from '../../Components/AddUpdateModal/AddUpdateModal';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Tooltip from '@mui/material/Tooltip';
import { useDispatch, useSelector } from 'react-redux';
import { getAllTodos, deleteTodo, updateTodo } from '../../Redux/Action';
import Loading from '../../Components/AddUpdateModal/Loading/Loading';
const UserList = () => {
    const dispatch = useDispatch();
    let [data, SetData] = useState([])
    let [user, SetUser] = useState({})
    let [ids, SetID] = useState()
    const [show, setShow] = useState(false);
    const handleShow = () => setShow(true);
    let [update, SetUpdate] = useState(false);

    const { todos, isLoading } = useSelector(state => state.todos);
    console.log(isLoading)


    const [open, setOpen] = React.useState(false);
    const handleClickOpen = (id) => {
        setOpen(true);
        SetID(id)

    };
    const handleClose = () => {
        setOpen(false);
    };

    const handleUpdate = (user) => {
        setShow(true);
        SetUpdate(true)
        SetUser(user)
        SetID(user.id)
    }

    const deleteuser = async (id) => {
        dispatch(deleteTodo(id))
        setOpen(false);
    }

    useEffect(() => {
        dispatch(getAllTodos());
    }, [show])

    return (
        <>



            <div className='container'>
                <AddUpdateModal user={user} ids={ids} SetUser={SetUser} show={show} setShow={setShow} update={update} data={data} SetUpdate={SetUpdate} SetData={SetData} />
                <div className='table-responsive my-3'>
                    <table className="table border text-center">
                        <thead className="table-dark">
                            <tr>
                                <th>
                                    {/* <input type="checkbox" name="allselect" checked={!todos.some((user) => user?.ischecked !== true)} onChange={handleChange} /> */}
                                </th>
                                <th scope="col">No.</th>
                                <th scope="col"> name</th>
                                <th scope="col">email</th>
                                <th scope="col">phone</th>
                                {/* <th scope="col">Image</th> */}
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody >
                            {isLoading && <Loading />}
                            {todos.length == 0 ?
                                <tr className='text-center'>
                                    <td colSpan="6"> No data found</td>
                                </tr>
                                :
                                todos.map((user, index) => {
                                    console.log(user)
                                    return (
                                        <tr key={user.id}>
                                            <td>
                                                {/* <input type="checkbox"
                                                    name={user.id}
                                                    checked={user?.ischecked || false}
                                                    onChange={handleChange} /> */}
                                            </td>
                                            <td>{user.id}</td>
                                            <td>{user.name}</td>
                                            <td>{user.email}</td>
                                            <td>{user.phone}</td>
                                            {/* <td><img src={user.avatar} alt="" /></td> */}
                                            <td>
                                                <Tooltip title="Edit">
                                                    <button className='btn btn-primary' onClick={() => handleUpdate(user)}>
                                                        <i class="fa-solid fa-pen-to-square"></i>
                                                    </button>
                                                </Tooltip>

                                                <Tooltip title="Delete">
                                                    <button className='btn btn-success mx-2'
                                                        color="success"
                                                        // startIcon={<DeleteIcon />}
                                                        // onClick={() => deleteuser(user.id)}>
                                                        onClick={() => handleClickOpen(user.id)}
                                                    >
                                                        <i class="fa-solid fa-trash"></i>
                                                    </button>
                                                </Tooltip>


                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                        <Dialog
                            open={open}
                            onClose={handleClose}
                            aria-labelledby="alert-dialog-title"
                            aria-describedby="alert-dialog-description"
                        >
                            <DialogTitle id="alert-dialog-title">
                                {"Are you sure ?"}
                            </DialogTitle>
                            <DialogContent>
                                <DialogContentText id="alert-dialog-description">
                                    It will delete permenantly
                                </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose}>No</Button>
                                <Button onClick={() => deleteuser(ids)} autoFocus>
                                    Yes
                                </Button>
                            </DialogActions>
                        </Dialog>

                    </table>
                </div>
                <button className="btn btn-danger my-3" onClick={handleShow}>
                    <AddIcon />
                </button>
            </div >
            <ToastContainer
                position="bottom-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />


        </>
    )
}

export default UserList
