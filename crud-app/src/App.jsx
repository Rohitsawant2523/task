import React from 'react'
import './App.css'
import { BrowserRouter as Router, Route, Routes, } from "react-router-dom";
import UserList from './Pages/User/UserList';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ChartJs from './Pages/User/ChartJs';

const App = () => {
  return (
    <>
      <UserList />
      {/* <ChartJs></ChartJs> */}
    </>
  )
}

export default App