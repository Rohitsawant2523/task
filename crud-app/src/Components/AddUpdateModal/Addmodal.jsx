import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { useNavigate } from 'react-router-dom';
import { Formik, Form, Field, ErrorMessage, useFormik } from 'formik';
import * as Yup from 'yup';
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { addNewTodo } from '../../Redux/Action';

const AddUpdateModal = ({ ids, user, SetUser, show, setShow, update, SetUpdate, data, SetData }) => {


    let dispatch = useDispatch();
    const handleClose = () => {
        setShow(false);
        SetUpdate(false)
        SetUser({})
    };
    const handlechange = (e) => {
        SetUser({ ...user, [e.target.name]: e.target.value })
    }
    const updateuserData = (e) => {
        console.log('update called')
        e.preventDefault(e);
        const updateUser = async () => {
            try {
                const response = await axios.put(`https://64e34f09bac46e480e789213.mockapi.io/user/${ids}`, user);
                console.log('User updated successfully:', response.data);
                toast.success('User updated successfully');
            } catch (error) {
                console.error('Error fetching data:', error);
                toast.error('Error updating user');
            }
        };
        updateUser();
        setShow(false);

    }

    const handleSubmit = (e) => {
        console.log('user created called')
        e.preventDefault();
        const postUser = async () => {
            try {
                const response = await axios.post('https://64e34f09bac46e480e789213.mockapi.io/user', user);
                console.log('User created successfully:', response.data);
                SetData([...data, user])
                toast.success('User created successfully');
            } catch (error) {
                console.error('Error fetching data:', error);
                toast.error('Error creating user');
            }
        };
        postUser();
        setShow(false);
    };
    return (
        <>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>  {update ? 'Update User' : 'Add User'}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form className='' onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <label htmlFor="exampleFormControlInput1" className="form-label">Name</label>
                            <input type="text"
                                className="form-control"
                                id="exampleFormControlInput1"
                                name='name' placeholder='name'
                                value={user.name}
                                onChange={handlechange}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleFormControlInput1" className="form-label">Email address</label>
                            <input type="email"
                                className="form-control"
                                id="exampleFormControlInput1"
                                name='email' placeholder='name'
                                value={user.email}
                                onChange={handlechange} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleFormControlInput1" className="form-label">Phone No.</label>
                            <input type="text"
                                className="form-control"
                                id="exampleFormControlInput1"
                                name='phone' placeholder='phone'
                                value={user.phone}
                                onChange={handlechange} />
                        </div>
                        <div className="mb-3 text-center">
                            {
                                update ?
                                    <button className='btn btn-success' onClick={updateuserData}>Update</button>
                                    :
                                    <button className='btn btn-success mx-2' >Add</button>
                            }
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        </>
    )
}

export default AddUpdateModal