import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { useNavigate } from 'react-router-dom';
import { Formik, Form, Field, ErrorMessage, useFormik } from 'formik';
import * as Yup from 'yup';
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { addNewTodo, updateTodo } from '../../Redux/Action';
const SignupSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Name is Required'),
    email: Yup.string().email('Invalid email').required('Email is Required'),
    phone: Yup.string()
        .matches(/^\d{10}$/, 'Phone number must be exactly 10 digits')
        .required('Phone number is Required'),
});


const AddUpdateModal = ({ ids, user, SetUser, show, setShow, update, SetUpdate, data, SetData }) => {


    let dispatch = useDispatch()

    const handleClose = () => {
        setShow(false);
        SetUpdate(false)
        SetUser({})
    };
    const handleChange = (e) => {
        SetUser({ ...user, [e.target.name]: e.target.value })
    }



    const modi = {
        name: user.name,
        email: user.email,
        phone: user.phone
    }

    const initialValues = {
        name: '',
        email: '',
        phone: ''
    }



    return (
        <>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>  {update ? 'Update User' : 'Add User'}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Formik
                        initialValues={Object.keys(user).length === 0 ? initialValues : modi}
                        validationSchema={SignupSchema}
                        onSubmit={values => {
                            console.log(values);
                            if (Object.keys(user).length === 0) {

                                dispatch(addNewTodo(values))
                                setShow(false);
                            }
                            else {

                                dispatch(updateTodo(values, ids))
                                setShow(false);
                            }
                        }}
                    >
                        {({ errors, touched }) => (
                            <Form>
                                <div className="mb-4">
                                    <label htmlFor="exampleFormControlInput1" >Name</label>
                                    <Field name="name"
                                        className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')}
                                        placeholder="Enter your Name" />
                                    {errors.name && touched.name ? (
                                        <span className='error-text'>{errors.name}</span>
                                    ) : null}
                                </div>
                                <div className="mb-4">
                                    <label htmlFor="exampleFormControlInput1" className="form-label">Email address</label>
                                    <Field name="email"
                                        className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')}
                                        placeholder="Enter your Email" />
                                    {errors.email && touched.email ? (
                                        <span className='error-text'>{errors.email}</span>
                                    ) : null}
                                </div>

                                <div className="mb-3">
                                    <label htmlFor="exampleFormControlInput1" className="form-label">Phone No.</label>
                                    <Field name="phone" type="text"
                                        className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} placeholder="Enter your phone number" />
                                    {errors.phone && touched.phone ?
                                        <span className='error-text'>{errors.phone}</span>
                                        : null}
                                </div>
                                {/* <div className="mb-3">
                                    <label htmlFor="exampleFormControlInput1" className="form-label">Phone No.</label>
                                    <Field name="phone" type="file"
                                        className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} placeholder="Enter your phone number" />
                                    {errors.phone && touched.phone ?
                                        <span className='error-text'>{errors.phone}</span>
                                        : null}
                                </div> */}
                                <div className="mb-3 text-center">
                                    <button className='btn btn-success mx-2' type='submit'>{update ? 'Update' : 'Add'}</button>
                                </div>
                            </Form>
                        )}

                    </Formik>

                    {/* <form className='' onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <label htmlFor="exampleFormControlInput1" className="form-label">Name</label>
                            <input type="text"
                                className="form-control"
                                id="exampleFormControlInput1"
                                name='name'
                                placeholder='name'
                                value={user.name}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleFormControlInput1" className="form-label">Email address</label>
                            <input type="email"
                                className="form-control"
                                id="exampleFormControlInput1"
                                name='email'
                                placeholder='name'
                                value={user.email}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleFormControlInput1" className="form-label">Phone No.</label>
                            <input type="text"
                                className="form-control"
                                id="exampleFormControlInput1"
                                name='phone'
                                placeholder='phone'
                                value={user.phone}
                                onChange={handleChange} />
                        </div>
                        <div className="mb-3 text-center">
                            {
                                update ?
                                    <button className='btn btn-success' onClick={updateuserData}>Update</button>
                                    :
                                    <button className='btn btn-success mx-2' type='submit'>Add</button>
                            }
                        </div>

                    </form> */}
                </Modal.Body>
            </Modal >
        </>
    )
}

export default AddUpdateModal