import React, { useState } from 'react'

const Image = () => {
    const [hasError, setHasError] = useState(false);
    console.log(hasError)

    const handleImageError = () => {
        setHasError(true);
    };
    return (
        <>
            <div>
                {hasError ? (
                    <p>Image failed to load</p>
                ) : (
                    <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRH9xTw4Y638PJTK9Uo1mc11HQ_zcVKrk9myo6S9Fm7Ng&s' alt="Image" onError={handleImageError} />
                )}
            </div>
        </>
    )
}

export default Image