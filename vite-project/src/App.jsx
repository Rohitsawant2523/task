import React from 'react';
import UseMemoHook from './Components/UseMemoHook/UseMemoHook';
import UseCallBackHook from './Components/UseCallBackHook/UseCallBackHook';
import UseEffectHook from './Components/UseEffectHook/UseEffectHook';
import Todo from './Components/Todo/Todos';
import './App.css';
import Login from './Pages/Login/Login';
import { BrowserRouter as Router, Route, Routes, } from "react-router-dom";
import Home from './Pages/Home/Home';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import About from './Pages/About';
import Navbar from './Components/Navbar/Navbar';
import Private from './Components/ProtectedRoute/Private';
import Stopwacth from './Components/Stopwatch/Stopwatch'
import Parent from './Components/Props/Parent';
import { UseReducer } from './Components/Usecontext/Usecontext';
import UseRef from './Components/UseRef/UseRef';
// import HigherOrder from './Components/HigherOrderComp/HigherOrder';
import Image from './image';
import Debounce from './Components/Debouncing/Debounce';
import Todos from './Components/UseReducer/UseReducer';
import UseLayoutEffect from './Components/uselayouteffect/UseLayoutEffect';
import UseFrom from './Components/UseFormHook/UseFrom';
import PdfDownload from './Components/PdfDownload/PdfDownload';
import Fileupload from './Components/FileUpload/Fileupload';
import FileUploadForm from './Components/FileUploadForm/FileUploadForm';
import FileCRUD from './Components/Files/Files';
// import Parent from './Components/State/Parent';
import PureComponent from './Components/PureComponent/PureComponent';
import ChatJs from './Components/ChartJs/ChatJs';
import FileDownload from './Components/FileDownload/FileDownload';
import RenderPdf from './Components/FileDownload/RenderPdf';
import DragFile from './Components/FileDownload/DragFile';
import GetLocation from './Components/GetLocation/GetLocation';
import UseQueryHook from './Components/UseQueryHook/UseQueryHook';
import ReactLogical from './Components/Reactlogical/ReactLogical';
import Counter from './Components/Scroll/Counter';
import Carousel from './Components/slickCarousal/SlickCarousal';
import Com from './Components/customHook/Com';
import Map from './Components/map';
import LocationMap from './Components/location';
import HigherOrder from './Components/HigherOrderComponent/HigherOrderComponent';
import Hover from './Components/HigheFun/Hover';
import Click from './Components/HigheFun/Click';
import ButtonTextCopy from './Components/ButtonTextCopy/ButtonTextCopy';
import Clipboard from './Components/ButtonTextCopy/ClipBoardCopy';
import ReactTabs from './Components/Tabs/ReactTabs';
import Skeletons from './Components/Skeleton/Skeleton';
import MultiStepForm from './Components/MultiStepForm';
import ProgressBar from './Components/ProgressBar/ProgressBar';
import EventPropogation from './Components/EventPropogation';
import Task from './Components/Task/Task';
import ChildA from './Components/HigherOrderComponent/ChildA';
import ChildB from './Components/HigherOrderComponent/ChildB';
const App = () => {
  return (
    <>
     <RenderPdf />
      {/* <DragFile></DragFile> */}

      {/* <ChildA></ChildA> */}
      {/* <ChildB></ChildB> */}
      {/* <HigherOrder /> */}

      {/* <Task></Task> */}
      {/* <EventPropogation /> */}
      {/* <ProgressBar /> */}
      {/* <MultiStepForm></MultiStepForm>
      <Skeletons />
      <ReactTabs /> */}
      {/* <Clipboard />
      <ButtonTextCopy></ButtonTextCopy>
      <Hover></Hover>
      <Click></Click> */}
      {/* <LocationMap /> */}
      {/* <Map /> */}
      {/* <Com /> */}
      {/* <Carousel /> */}
      {/* <ReactLogical></ReactLogical> */}
      {/* <UseQueryHook /> */}
      {/* <GetLocation />
      <RenderPdf />
      <ChatJs></ChatJs>
      <PdfDownload />
      <FileDownload />
      <Counter /> */}

      {/* <Parent></Parent> */}
      {/* <FileCRUD></FileCRUD> */}
      {/* <FileUploadForm /> */}
      {/* <Fileupload></Fileupload> */}
      {/* <UseFrom></UseFrom> */}
      {/* <UseLayoutEffect /> */}
      {/* <Debounce /> */}
      {/* <Todos /> */}
      {/* <Image /> */}r
      {/* <HigherOrder></HigherOrder> */}
      {/* <UseRef></UseRef> */}
      <UseReducer >
        <Parent />
      </UseReducer>
      {/* <UseMemoHook></UseMemoHook> */}
      {/* <UseCallBackHook></UseCallBackHook>/ */}
      {/* <UseEffectHook></UseEffectHook> */}
      {/* <Todo></Todo> */}
      {/* <Stopwacth /> */}
      {/* <Router>
        <Navbar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/about' element={
            <Private>
              <About />
            </Private>
          }>
          </Route>
          <Route path='/login' element={<Login />} />
        </Routes>
      </Router> */}

    </>
  )
}

export default App