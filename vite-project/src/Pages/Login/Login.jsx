import React, { useState, useEffect } from 'react';
import Home from '../Home/Home';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const Login = () => {
    let [user, SetUser] = useState({ email: "", password: '' })
    let emaildata = localStorage.getItem('email')
    let passwordata = localStorage.getItem('password')
    const loginfun = () => {
        if (user.email === 'abc@gmail.com' && user.password == '1234567') {
            localStorage.setItem('email', 'abc@gmail.com')
            localStorage.setItem('password', '1234567')
        }
    }
    const handleChange = (e) => {
        SetUser({ ...user, [e.target.name]: e.target.value })
    }
    return (
        <>
            {emaildata && passwordata ?
                <Home></Home> :
                <>
                    <h1>Login Here</h1>
                    <form onSubmit={loginfun}>
                        <input type="text" name='email' placeholder='email' value={user.email} onChange={handleChange} />
                        <input type="text" name='password' placeholder='password' value={user.password} onChange={handleChange} />
                        <button>submit</button>
                    </form>
                </>
            }
            <ToastContainer />
        </>
    )
}

export default Login