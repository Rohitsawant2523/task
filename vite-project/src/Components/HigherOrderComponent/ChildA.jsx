import React from 'react';
import MainHIgherOrder from './MainHIgherOrder';

const ChildA = ({ count,  incresCou }) => {
    return (
        <>
            <p>{count}</p>
            <button onClick={incresCou}>ChildA</button>
        </>
    )
}

export default MainHIgherOrder(ChildA)