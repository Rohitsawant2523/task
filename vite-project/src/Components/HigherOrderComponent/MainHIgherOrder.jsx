import React, { useState } from 'react'

const MainHIgherOrder = (WrappedComponent) => {
    function main() {
        let [count, setCount] = useState(0)

        const incresCou = () => {
            setCount(count + 1)
        }

        return <WrappedComponent count={count}  incresCou={incresCou} />

    }

    return main
}

export default MainHIgherOrder