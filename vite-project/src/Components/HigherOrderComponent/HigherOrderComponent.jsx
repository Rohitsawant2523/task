import React, { useState } from 'react';

const withCounter = (WrappedComponent) => {
    return function WithCounter(props) {
        console.log('props', props)
        const [counter, setCounter] = useState(0);

        console.log('counter', counter)
        const incrementCounter = () => {
            setCounter(counter + 1);
        };

        return <WrappedComponent counter={counter} incrementCounter={incrementCounter} {...props} />;
    };
};

function DisplayCounter({ counter, incrementCounter }) {
    return (
        <div>
            <p>Counter: {counter}</p>
            <button onClick={incrementCounter}>Increment Counter</button>
        </div>
    );
}

function DisplayCounterHover({ counter, incrementCounter }) {
    return (
        <div>
            <p>Counter: {counter}</p>
            <button onMouseOver={incrementCounter}>Increment Counter</button>
        </div>
    );
}


const CounterWithHOC = withCounter(DisplayCounterHover);

function HigherOrder() {
    return (
        <div>
            {/* <h5>Functional Higher-Order Component Example</h5> */}
            <CounterWithHOC />
            {/* <DisplayCounterHover /> */}
            {/* <DisplayCounter /> */}

        </div>
    );
}

export default HigherOrder;




