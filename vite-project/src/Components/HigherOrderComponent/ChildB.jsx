import React from 'react';
import MainHIgherOrder from './MainHIgherOrder';

const ChildB = ({ count, incresCou }) => {
    return (
        <>
            <p>{count}</p>
            <button onClick={incresCou}>ChildA</button>
        </>
    )
}

export default MainHIgherOrder(ChildB)