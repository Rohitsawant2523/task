// import React, { useState } from 'react';
// import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
// import Table from './Table';

// import './ReactTabs.css'

// const ReactTabs = () => {
// const [tableData, setTableData] = useState([
//     {
//         name: 'John Doe',
//         email: 'john@example.com',
//         phone: '123-456-7890',
//         gender: 'Male',
//         city: '',
//         error: false,
//     },
//     {
//         name: 'Jane Smith',
//         email: 'jane@example.com',
//         phone: '987-654-3210',
//         gender: 'Female',
//         city: '',
//         error: false,
//     },
// ]);

// const columns = [
//     {
//         name: 'Name',
//         selector: (row) => row.name,
//         sortable: true,
//     },
//     {
//         name: 'Email',
//         selector: (row) => row.email,
//         sortable: true,
//     },
//     {
//         name: 'Phone',
//         selector: (row) => row.phone,
//         sortable: true,
//     },
//     {
//         name: 'Gender',
//         selector: (row) => row.gender,
//         sortable: true,
//     },
//     {
//         name: 'City',
//         cell: (row) => {
//             return (
//                 <select
//                     value={row.city}
//                     onChange={(e) => handleCityChange(row, e.target.value)}
//                 >
//                     <option value="">Select</option>
//                     <option value="New York">New York</option>
//                     <option value="Los Angeles">Los Angeles</option>
//                     <option value="Chicago">Chicago</option>
//                     <option value="Houston">Houston</option>
//                     <option value="Miami">Miami</option>
//                 </select>
//             );
//         },
//     },
//     {
//         name: "Action",
//         cell: (row) => {
//             return (
//                 <>
//                     <button
//                         className='btn btn-danger'
//                         type='button'
//                         onClick={() => handleSubmit(row)}
//                     >
//                         Submit
//                     </button>
//                     {row.error && <span style={{ color: 'red' }}>Select a city</span>}
//                 </>
//             );
//         },
//     },
// ];

// const handleCityChange = (row, value) => {
//     const updatedData = [...tableData];
//     console.log('updatedData', updatedData)
//     const updatedRow = { ...row, city: value, error: false }; // Clear the error
//     console.log('updatedRow', updatedRow)
//     const index = updatedData.indexOf(row);
//     updatedData[index] = updatedRow;
//     setTableData(updatedData);
// };



// const handleSubmit = (row) => {
//     console.log('row', row)
//     if (row.city === '') {
//         const updatedData = [...tableData];
//         console.log('updatedData', updatedData)
//         const updatedRow = { ...row, error: true };
//         const index = updatedData.indexOf(row);
//         console.log('index', index)
//         updatedData[index] = updatedRow;
//         setTableData(updatedData);
//     } else {
//         const updatedData = [...tableData];
//         const updatedRow = { ...row, error: false };
//         const index = updatedData.indexOf(row);
//         updatedData[index] = updatedRow;
//         setTableData(updatedData);
//     }
// };

//     return (
//         <>
//             <div className='maisn'>
//                 <Tabs>
//                     <TabList className='d-flex gx-5 TabList'>
//                         <Tab className='tabList'>Table</Tab>
//                         <Tab className='tabList'>Content</Tab>
//                     </TabList>

//                     <TabPanel>
//                         <Table columns={columns} data={tableData} />
//                     </TabPanel>
//                     <TabPanel>
//                         <h2>Any content 2</h2>
//                     </TabPanel>
//                 </Tabs>
//             </div>
//         </>
//     );
// }

// export default ReactTabs;


import React, { useState } from 'react';
import './ReactTabs.css'
import Table from './Table';

function ReactTabs() {
    const [activeTab, setActiveTab] = useState(0);
    const [maxTabCount, setMaxTabCount] = useState(2);
    const [inputError, setInputError] = useState(false);
    const [selectError, setSelectError] = useState(false);


    const handleTabClick = (index) => {
        console.log('index', index)
        console.log('activeTab', activeTab)
        console.log('maxTabCount', maxTabCount)


        if (index === activeTab) return;
        if (index === activeTab + 1) {
            if (activeTab + 1 >= maxTabCount) {
                setMaxTabCount(activeTab + 2);
            }
        } else if (index === activeTab - 1) {
            if (activeTab - 1 < maxTabCount - 2) {
                setMaxTabCount(activeTab - 1);
            }
        }
        setActiveTab(index);
    };

    const renderNextButton = () => {
        console.log('activeTab', activeTab)
        if (activeTab === maxTabCount - 1) {
            return (
                <button onClick={() => handleSubmit(tableData[activeTab])}>
                    Submit
                </button>
            );
        } else {
            return (
                <button onClick={() => handleTabClick(activeTab + 1)}>
                    Next
                </button>
            );
        }
    };


    const [tableData, setTableData] = useState([
        {
            name: 'John Doe',
            email: 'john@example.com',
            phone: '123-456-7890',
            gender: 'Male',
            city: '',
            input: '',
            error: {
                city: false,
                input: false,
            },
        },
        {
            name: 'Jane Smith',
            email: 'jane@example.com',
            phone: '987-654-3210',
            gender: 'Female',
            city: '',
            input: '',
            error: {
                city: false,
                input: false,
            },
        },
    ]);
    const columns = [
        {
            name: 'Name',
            selector: (row) => row.name,
            sortable: true,
        },
        {
            name: 'Email',
            selector: (row) => row.email,
            sortable: true,
        },
        {
            name: 'Phone',
            selector: (row) => row.phone,
            sortable: true,
        },
        {
            name: 'Gender',
            selector: (row) => row.gender,
            sortable: true,
        },
        {
            name: 'City',
            cell: (row) => {
                return (
                    <select
                        value={row.city}
                        onChange={(e) => handleCityChange(row, e.target.value)}
                        className={row.error.city ? 'error' : ''}
                    >
                        <option value="">Select</option>
                        <option value="New York">New York</option>
                        <option value="Los Angeles">Los Angeles</option>
                        <option value="Chicago">Chicago</option>
                        <option value="Houston">Houston</option>
                        <option value="Miami">Miami</option>
                    </select>
                );
            },
        },
        {
            name: 'Input Field',
            cell: (row) => (
                <input
                    type="text"
                    value={row.input}
                    onChange={(e) => handleInputFieldChange(row, e.target.value)}
                    className={row.error.input ? 'error' : ''}
                />
            ),
        },
        {
            name: "Action",
            cell: (row) => {
                return (
                    <>
                        <button
                            className='btn btn-danger'
                            type='button'
                            onClick={() => handleSubmit(row)}
                        >
                            Submit
                        </button>
                    </>
                );
            },
        },
    ];

    const handleCityChange = (row, value) => {
        const updatedData = [...tableData];
        const updatedRow = { ...row, city: value, error: { ...row.error, city: false } };
        const index = updatedData.indexOf(row);
        updatedData[index] = updatedRow;
        setTableData(updatedData);
    };

    const handleInputFieldChange = (row, value) => {
        const updatedData = [...tableData];
        console.log('updatedData', updatedData)
        const updatedRow = { ...row, input: value, error: { ...row.error, input: false } };
        console.log('updatedRow', updatedRow)
        const index = updatedData.indexOf(row);
        console.log('index', index)
        updatedData[index] = updatedRow;
        setTableData(updatedData);
    };


    console.log('tableData', tableData)
    const handleSubmit = (row) => {
        console.log('row', row)
        if (row.city === '' || row.input === '') {
            const updatedData = [...tableData];
            const updatedRow = {
                ...row,
                error: {
                    city: row.city === '',
                    input: row.input === '',
                },
            };
            const index = updatedData.indexOf(row);
            updatedData[index] = updatedRow;
            setTableData(updatedData);
        } else {
            const updatedData = [...tableData];
            const updatedRow = { ...row, error: { city: false, input: false } };
            const index = updatedData.indexOf(row);
            updatedData[index] = updatedRow;
            setTableData(updatedData);
        }
    };



    return (
        <div className="tab-panel container mt-5">
            <ul className="tab-list">
                <li
                    className={activeTab === 0 ? "active" : ""}
                    onClick={() => handleTabClick(0)}
                >
                    Tab 1
                </li>
                <li
                    className={activeTab === 1 ? "active" : ""}
                    onClick={() => handleTabClick(1)}
                >
                    Tab 2
                </li>
            </ul>

            <div className="tab-content">
                <div
                    className={`tab-pane ${activeTab === 0 ? "active" : ""}`}
                >
                    <Table columns={columns} data={tableData} />

                </div>
                <div
                    className={`tab-pane ${activeTab === 1 ? "active" : ""}`}
                >
                    Content for Tab 2
                </div>

            </div>
            {/* <button onClick={() => handleTabClick(activeTab + 1)}>Next</button> */}
            {renderNextButton()}

        </div>
    );
}

export default ReactTabs;
