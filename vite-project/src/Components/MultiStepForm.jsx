import React, { useState } from 'react';

function MultiStepForm() {
  const [formData, setFormData] = useState({
    step1: '',
    step2: '',
    step3: '',
  });

  const handleStepChange = (step, value) => {
    setFormData({ ...formData, [step]: value });
  };

  const handleSubmit = () => {
    console.log(formData);
  };

  return (
    <div>
      {formData.step1 === '' ? (
        <Step1 onChange={value => handleStepChange('step1', value)} />
      ) : formData.step2 === '' ? (
        <Step2 onChange={value => handleStepChange('step2', value)} />
      ) : formData.step3 === '' ? (
        <Step3 onChange={value => handleStepChange('step3', value)} />
      ) : (
        <div>
          <h2>Form Submitted!</h2>
          <button onClick={handleSubmit}>Submit</button>
        </div>
      )}
    </div>
  );
}

function Step1({ onChange }) {
  return (
    <div>
      <h2>Step 1</h2>
      <input
        type="text"
        placeholder="Step 1 Data"
        onChange={e => onChange(e.target.value)}
      />
      <button onClick={() => onChange('next')}>Next</button>
    </div>
  );
}

function Step2({ onChange }) {
  return (
    <div>
      <h2>Step 2</h2>
      <input
        type="text"
        placeholder="Step 2 Data"
        onChange={e => onChange(e.target.value)}
      />
      <button onClick={() => onChange('next')}>Next</button>
    </div>
  );
}

function Step3({ onChange }) {
  return (
    <div>
      <h2>Step 3</h2>
      <input
        type="text"
        placeholder="Step 3 Data"
        onChange={e => onChange(e.target.value)}
      />
      <button onClick={() => onChange('next')}>Next</button>
    </div>
  );
}

export default MultiStepForm;
