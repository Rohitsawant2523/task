import React, { useState } from 'react'

const Child = ({ data, setData, sendDataToParent }) => {
    let [toggle, setToggle] = useState(true)
    const handlechange = () => {
        setData('red')
        setToggle(!toggle)
    }
    const callfun = () => {
        sendDataToParent('green');
    }

    return (
        <>
            <h1>{toggle ? 'red' : 'blue'}</h1>
            <button onClick={handlechange}>update</button>
            <button onClick={callfun}>click</button>
        </>
    )
}

export default Child