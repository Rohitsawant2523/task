import React, { useState } from 'react';
import Child from './Child';

const Parent = () => {
    let [data, setData] = useState('pk')
    const [dataFromChild, setDataFromChild] = useState('yellow');



    // Callback function to receive data from the child
    const receiveDataFromChild = (data) => {
        console.log(data)
        setDataFromChild(data);
    };
    return (
        <>
            <p>{dataFromChild}</p>
            <Child data={data} setData={setData} sendDataToParent={receiveDataFromChild} ></Child>
        </>
    )
}

export default Parent