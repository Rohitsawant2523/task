import React, { useState, useEffect } from 'react';
import axios from 'axios';


const Fileupload = () => {
    return (
        <>
            <FileUploader />
        </>
    )
}

export default Fileupload

const FileUploader = () => {
    const [selectedFile, setSelectedFile] = useState(null);

    const handleFileChange = (e) => {
        const file = e.target.files[0];
        setSelectedFile(file);
    };

    // const handleUpload = () => {
    //     // Implement the file upload logic here
    //     if (selectedFile) {
    //         // You can use the selectedFile for further processing, such as sending it to a server
    //         console.log('Selected file:', selectedFile);
    //     }
    // };
    const handleUpload = () => {
        if (selectedFile) {
            const formData = new FormData();
            formData.append('file', selectedFile);
            console.log(formData)

            axios.post('https://api.escuelajs.co/api/v1/files/upload', formData)
                .then((response) => {
                    // Handle the response from the server
                    console.log('File uploaded successfully:', response.data);
                })
                .catch((error) => {
                    console.error('Error uploading file:', error);
                });
        }
    };

    useEffect(() => {
        const getData = async()
    })

    return (
        <div>
            <h2>File Uploader</h2>
            <input type="file" onChange={handleFileChange} />
            <button onClick={handleUpload}>Upload</button>
        </div>
    );
};