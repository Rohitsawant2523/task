import React, { useState } from 'react';

const HigherOrder = () => {
    return (
        <>
            {/* <Counter /> */}
            <Hoc cmp={Counter} />
        </>
    )
}

function Hoc(props) {
    return (
        <>
            <div>
                <props.cmp />
            </div>
        </>
    )
}

function Counter() {
    let [count, SetCount] = useState(0)
    return (
        <>
            <h1>{count}</h1>
            <button onClick={() => SetCount(count + 1)}>Click</button>
        </>
    )
}


export default HigherOrder