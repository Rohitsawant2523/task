import React from 'react';
import Slider from 'react-slick';

const Carousel = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        autoplay: true,        // Add this line for autoplay
        slidesToShow: 3, // Number of slides to show at once
        slidesToScroll: 1, // Number of slides to scroll at once
    };

    return (
        <div>
            <h2>React Slick Carousel Example</h2>
            <Slider {...settings}>
                <div className='mains'>
                    <img src="image1.jpg" alt="Image 1" />
                </div>
                <div className='mains'>
                    <img src="image2.jpg" alt="Image 2" />
                </div>
                <div className='mains'>
                    <img src="image3.jpg" alt="Image 3" />
                </div>
                {/* Add more slides as needed */}
            </Slider>
        </div>
    );
};

export default Carousel;
