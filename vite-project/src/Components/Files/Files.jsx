import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Table, Button, Form } from 'react-bootstrap';

const FileCRUD = () => {
    const [files, setFiles] = useState([]);
    const [file, setFile] = useState(null);
    const [title, setTitle] = useState('');
    const [id, setId] = useState(null);

    useEffect(() => {
        // Fetch the list of files from the API
        axios.get('https://jsonplaceholder.typicode.com/photos').then((response) => {
            setFiles(response.data);
        });
    }, []);

    const handleFileChange = (e) => {
        setFile(e.target.files[0]);
    };

    const handleTitleChange = (e) => {
        setTitle(e.target.value);
    };

    const handleIdChange = (e) => {
        setId(e.target.value);
    };

    const handleUpload = () => {
        // Simulate file upload to the API (replace with your own API endpoint)
        const formData = new FormData();
        formData.append('file', file);

        axios.post('https://jsonplaceholder.typicode.com/photos', formData).then((response) => {
            setFiles([...files, response.data]);
        });
    };

    const handleUpdate = async (id) => {
        console.log(id)
        // Simulate updating the file title (replace with your own API endpoint)
        await axios.put(`https://jsonplaceholder.typicode.com/photos/${id}`, { title }).then(() => {
            const updatedFiles = files.map((f) => (f.id === id ? { ...f, title } : f));
            setFiles(updatedFiles);
            setTitle('');
            setId(null);
        });
    };
    console.log(files)

    const handleDelete = (id) => {
        // Simulate file deletion (replace with your own API endpoint)
        axios.delete(`https://jsonplaceholder.typicode.com/photos/${id}`).then(() => {
            const updatedFiles = files.filter((f) => f.id !== id);
            setFiles(updatedFiles);
        });
    };

    return (
        <div>

            <Form>
                <Form.Group controlId="file">
                    <Form.Label>Upload File</Form.Label>
                    <Form.Control type="file" onChange={handleFileChange} />
                </Form.Group>
                <Button variant="primary" onClick={handleUpload}>
                    Upload
                </Button>
            </Form>

            <Form>
                <Form.Group controlId="title">
                    <Form.Label>Update Title (by ID)</Form.Label>
                    <Form.Control type="number" placeholder="Enter ID" onChange={handleIdChange} />
                    <Form.Control type="text" placeholder="Enter Title" onChange={handleTitleChange} />
                </Form.Group>
                <Button variant="info" onClick={handleUpdate}>
                    Update
                </Button>
            </Form>

            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {files.map((file) => (
                        <tr key={file.id}>
                            <td>{file.id}</td>
                            <td>{file.title}</td>
                            <td>
                                {/* <Button variant="danger" onClick={() => handleDelete(file.id)}>
                                    Delete
                                </Button> */}
                                <Button variant="info" onClick={() => handleUpdate(file.id)}>
                                    Update
                                </Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </div>
    );
};

export default FileCRUD;
