// import React from 'react';

// const EventPropagation = () => {
//     const handleChildClick = (e) => {
//         // e.stopPropagation();
//         alert('div child');

//     };

//     return (
//         <>
//             <div onClick={() => alert('div parent')}>
//                 <div onClick={handleChildClick}>
//                     <button>Click</button>
//                 </div>
//             </div>
//         </>
//     );
// }

// export default EventPropagation;

import React from 'react';

const EventPropagation = () => {
    const handleParentClick = () => {
        alert('div parent'); // This will be alerted first.
    };

    const handleChildClick = (e) => {
        alert('div child');
    };

    return (
        <>
            <div onClick={handleParentClick}>
                <div onClick={handleChildClick}>
                    <button>Click</button>
                </div>
            </div>
        </>
    );
}

export default EventPropagation;
