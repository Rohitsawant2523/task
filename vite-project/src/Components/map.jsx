import React, { useState } from 'react'

const Map = () => {

    const [mapLocation, setMapLocation] = useState(
        'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d120441.22681702749!2d72.7445713659842!3d19.37831549990425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7ae956bc1587b%3A0x864f53a94baa5145!2sVasai-Virar%2C%20Maharashtra!5e0!3m2!1sen!2sin!4v1697706064043!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade'
    );



    const handleGetUserLocation = () => {
        if (navigator.geolocation) {
            console.log('navigator.geolocation', navigator.geolocation)
            navigator.geolocation.getCurrentPosition((position) => {
                console.log('position', position)
                const { latitude, longitude } = position.coords;
                const newMapSrc = `https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15089.900425496879!2d${longitude}!3d${latitude}!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1en!2sin!4v1697704424297!5m2!1sen!2sin`;
                setMapLocation(newMapSrc);
            }, (error) => {
                console.error('Error getting user location:', error);
            });
        } else {
            console.error('Geolocation is not supported by your browser.');
        }
    };
    return (
        <>
            <iframe
                src={mapLocation}
                width="600"
                height="450"
                className="map-iframe"
                allowFullScreen
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
            ></iframe>
            <button onClick={handleGetUserLocation}>Get My Location</button>
        </>
    )
}

export default Map