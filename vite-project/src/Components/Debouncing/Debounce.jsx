import React, { useState } from 'react'

const Debounce = () => {
    const [inputValue, setInputValue] = useState('');// Function to perform the actual action you want to debounce 
    const handleInputChange = (value) => {
        console.log('Input value:', value);
    }
    // Perform your action here, e.g., making API calls, updating state, etc.  };  // Debounce function  
    const debounce = (func, delay) => {
        let timer;
        return (...args) => {
            clearTimeout(timer);
            timer = setTimeout(() =>
                func(...args)
                , delay);
        };
    };
    // Create a debounced version of the input change handler  
    const debouncedHandleInputChange = debounce(handleInputChange, 2000); // Adjust the delay as needed
    const onInputChange = (event) => {
        const newValue = event.target.value;
        setInputValue(newValue);
        debouncedHandleInputChange(newValue);
    };
    return (
        <>
            <input
                type="text"
                value={inputValue}
                onChange={onInputChange}
                placeholder="Type something..."
            />
            <p>{inputValue}</p>
        </>
    )

}

export default Debounce