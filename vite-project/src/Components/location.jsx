import React, { useEffect } from 'react';

const LocationMap = () => {
    const handleGetLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                const userLocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };

                const map = new window.google.maps.Map(document.getElementById('map'), {
                    center: userLocation,
                    zoom: 14,
                });

                new window.google.maps.Marker({
                    position: userLocation,
                    map: map,
                    title: 'Your Location',
                });
            }, (error) => {
                console.error('Error getting user location:', error);
            });
        } else {
            console.error('Geolocation is not supported by your browser.');
        }
    };

    useEffect(() => {
        const script = document.createElement('script');
        script.src = `https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places`;
        script.async = true;
        script.defer = true;

        script.onload = () => {
            handleGetLocation();
        };

        document.head.appendChild(script);

        // Clean up the script element on unmount
        return () => {
            document.head.removeChild(script);
        };
    }, []);

    return (
        <div>
            <button onClick={handleGetLocation}>Get My Location</button>
            <div id="map" style={{ width: '100%', height: '400px' }}></div>
        </div>
    );
}

export default LocationMap;
