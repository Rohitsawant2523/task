import React, { useState } from 'react'

const ReactLogical = () => {
    const [selectedNumbers, setSelectedNumbers] = useState([]);
    const numbers = [1, 2, 3, 4, 5];

    const handleCheckboxChange = (number) => {
        if (selectedNumbers.includes(number)) {
            setSelectedNumbers(selectedNumbers.filter((n) => n !== number));
        } else {
            setSelectedNumbers([...selectedNumbers, number]);
        }
    };
    console.log('selectedNumbers', selectedNumbers)
    return (
        <>
            <div className='d-flex'>

                <div>
                    <li>1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>4</li>
                </div>
                <div>
                    <li>11</li>
                    <li>22</li>
                    <li>33</li>
                    <li>44</li>
                </div>
            </div>
            <ul>
                {numbers.map((number) => (
                    <li key={number}>
                        <input
                            type="checkbox"
                            value={number}
                            checked={selectedNumbers.includes(number)}
                            onChange={() => handleCheckboxChange(number)}
                        />
                        {number}
                    </li>
                ))}
            </ul>

        </>
    )
}

export default ReactLogical