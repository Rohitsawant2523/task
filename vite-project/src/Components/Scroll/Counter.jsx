import React, { useState, useEffect } from 'react';

const Counter = () => {
  const [counter, setCounter] = useState(0);

  const handleScroll = () => {
    if (window.scrollY < 80) {
      setCounter((prevCounter) => prevCounter + 1);
    }
  };

  console.log('counter', counter)
  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <div>
      <h2>Scroll-Up Increment Counter:</h2>
      <p>Count: {counter}</p>
    </div>
  );
};

export default Counter;
