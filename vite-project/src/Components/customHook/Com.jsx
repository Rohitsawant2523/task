import React from 'react'
import CustomHook from './CustomHook'
const Com = () => {
    let [data] = CustomHook('https://dummyjson.com/users')
    console.log('data', data?.users)
    return (
        <>

            {
                data?.users.map((ele) => {
                    return (
                        <p>{ele.email}</p>
                    )
                })
            }
        </>
    )
}

export default Com