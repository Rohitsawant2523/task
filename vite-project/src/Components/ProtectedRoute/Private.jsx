import React from 'react'
import { Navigate, Outlet } from 'react-router-dom';

const Private = ({ children }) => {
    let emaildata = localStorage.getItem('email')
    let passwordata = localStorage.getItem('password')
    console.log(emaildata, passwordata)
    if (emaildata !== 'abc@gmail.com' && passwordata !== '1234567') {
        return <Navigate to="/login" replace></Navigate>;
    }
    return children
}

export default Private
