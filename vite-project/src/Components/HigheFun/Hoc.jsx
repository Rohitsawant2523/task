import React, { useState } from 'react'

const Hoc = (Component) => {
    function Wrap() {
        const [state, setState] = useState(0)
        return <Component state={state} setState={setState} />
    }
    return Wrap
}

export default Hoc