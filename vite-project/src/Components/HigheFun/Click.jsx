import React from 'react'
import Hoc from './Hoc'

const Click = ({ state, setState }) => {
    return (
        <div>Click

            <button onClick={() => setState(state++)}>button{state}</button>
        </div>
    )
}

export default Hoc(Click)