import React from 'react'
import Hoc from './Hoc'

const Hover = ({ state, setState }) => {
    return (
        <div>
            Hover
            <button onMouseOver={() => setState(state++)}>Hover{state}</button>
        </div>
    )
}

export default Hoc(Hover)