import React, { useState } from "react";
import { Document, Page, pdfjs } from "react-pdf";
import './RenderPdf.css'
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
import { FileUploader } from "react-drag-drop-files";

function RenderPdf() {
  const [selectedFile, setSelectedFile] = useState(null);

  const onFileChange = (event) => {
    const file = event.target.files[0];
    setSelectedFile(file);
  };

  console.log('selectedFile', selectedFile)

  const fileTypes = ["JPG", "PNG", "GIF"];

  const [file, setFile] = useState(null);
  const handleChange = (file) => {
    setFile(file);
  };
  console.log('file', file)
  return (

    <>
      <FileUploader handleChange={handleChange} name="file" label={file ? file.name : 'please upload file here'} types={fileTypes} />
      <div>
        <h1>PDF Viewer</h1>
        <div>
          <input
            type="file"
            //   accept=".pdf"
            onChange={onFileChange}
          />
        </div>
        <div>

        </div>
        <div>
          {/* {selectedFile && ( */}
          <>
            <div className="mainPdfDiv">
              <Document file={selectedFile} className="innerMain">
                <Page pageNumber={1} />
              </Document>
            </div>
          </>

          {/* )} */}
        </div>
      </div>
    </>
  );
}

export default RenderPdf;




