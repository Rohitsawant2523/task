import React, { useState } from "react";
import { Document, Page, pdfjs } from "react-pdf";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
import './RenderPdf.css';
import { useQuery } from "react-query";
function DragFile() {


    const [uploadedFile, setUploadedFile] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);
    const [numPages, setNumPages] = useState(null);

    const handleFileDrop = (event) => {
        event.preventDefault();
        const file = event.dataTransfer.files[0];
        if (file) {
            setUploadedFile(file);
        }
    };

    const handleFileSelect = (event) => {
        const file = event.target.files[0];
        if (file) {
            setUploadedFile(file);
            setPageNumber(1); 
        }
    };

    const onDocumentLoadSuccess = ({ numPages }) => {
        setNumPages(numPages);
    };

    const nextPage = () => {
        if (pageNumber < numPages) {
            setPageNumber(pageNumber + 1);
        }
    };
    const previousPage = () => {
        if (pageNumber > 1) {
            setPageNumber(pageNumber - 1);
        }
    };
    console.log('uploadedFile', uploadedFile)

    return (
        <div
            onDragOver={(e) => e.preventDefault()}
            onDrop={handleFileDrop}
            style={{
                border: "2px dashed #ccc",
                padding: "20px",
                height: '450px',
                width: '450px',
                textAlign: "center",
                cursor: "pointer",
                overflow: 'scroll'
            }}
        >
            {
                uploadedFile == null ?
                    <>
                        <input
                            type="file"
                            accept=".pdf"
                            onChange={handleFileSelect}
                            style={{ display: "none" }}
                        />
                        <p>Drag and drop a file here or</p>
                        <button onClick={() => document.querySelector("input[type='file']").click()}>
                            Select a File
                        </button>
                    </>
                    :
                    <Document
                        file={uploadedFile}
                        onLoadSuccess={onDocumentLoadSuccess}
                        className="innerMain">
                        <Page pageNumber={pageNumber} />
                    </Document>
            }
            {numPages && (
                <div>
                    <p>
                        Page {pageNumber} of {numPages}
                    </p>
                    <button onClick={previousPage}>Previous Page</button>
                    <button onClick={nextPage}>Next Page</button>
                </div>
            )}
        </div>
    );
}

export default DragFile;
