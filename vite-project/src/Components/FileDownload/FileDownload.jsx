import React from "react";

function FileDownload() {
    const downloadFile = () => {
        // const fileURL = "https://example.com/sample-file.pdf"; // Replace with your file URL
        const fileURL = "https://www.africau.edu/images/default/sample.pdf";

        window.open(fileURL, "_self");
    };

    return (
        <div>
            <button onClick={downloadFile}>Download File</button>
        </div>
    );
}

export default FileDownload;
